<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Api extends CI_Model{

    /* CRUD Data */
    public function get_data($table) {
        return $this->db->get($table);
    }
    public function insert_data($table,$data) {
        $this->db->insert($table,$data);
    }
	public function update_data($table, $data, $id, $key) {
        $this->db->where($key, $id);
        $this->db->update($table, $data);
    }

    public function delete_data($table, $id, $key) {
        $this->db->where($key, $id);
        $this->db->delete($table);
    }

    public function get_data_byid($table, $id, $key) {
        $this->db->where($key, $id);
        return $this->db->get($table);
    }
    /***********************************************/
	
	/* Autentikasi @#$?^&*();:?/<>*/
	public function cek_user($username) {
        $query = "SELECT count(*) as count from users WHERE username = '$username'";
        return $this->db->query($query);
    }
	
	public function cek_email($email) {
        $query = "SELECT count(*) as count from users WHERE email = '$email'";
        return $this->db->query($query);
    }
	/***********************************************/
}