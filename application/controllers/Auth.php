 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

//use chriskacerguis\RestServer\RestController;
//use \Firebase\JWT\JWT;
//use \Firebase\JWT\Key;

class Auth extends CI_Controller {
	
	public static $table = 'users';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Api');

    }
    
    public function json_respone($respone) {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        header('Content-type: application/json');
        echo json_encode($respone, JSON_PRETTY_PRINT);
    
    }
	
	public function registrasi() {
		
		if($_SERVER['REQUEST_METHOD'] == 'POST'){

			$post = (array)json_decode(file_get_contents('php://input'));
			if(empty($post)) {
				$respone['value'] = "0";
				$respone['message'] = "Form harus di isi lengkap";
			} else {
				$name = trim($post['name']);
				$email = trim($post['email']);
				$username = trim($post['username']);
				$password = trim($post['password']);
				
				if(empty($name)) {
					$respone['value'] = "0";
					$respone['message'] = "Nama harus di isi";
				} elseif (empty($username)) {
					$respone['value'] = "0";
					$respone['message'] = "Username harus di isi";
				} elseif (empty($password)) {
					$respone['value'] = "0";
					$respone['message'] = "Password harus di isi";
				} else {
					if (strlen($username) >= 0 && strlen($username) < 6) {
						$respone['value'] = "0";
						$respone['message'] = "Username terlalu pendek";

					} elseif (strlen($username) >= 0 && strlen($username) > 20) {
						$respone['value'] = "0";
						$respone['message'] = "Username terlalu panjang";

					} elseif (strlen($password) >= 0 && strlen($password) < 6) {
						$respone['value'] = "0";
						$respone['message'] = "Password terlalu pendek";

					} elseif (strlen($password) >= 0 && strlen($password) > 20) {
						$respone['value'] = "0";
						$respone['message'] = "Password terlalu panjang";

					} else {

						$permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@#$?^&*();:?/<>';
						$key = substr(str_shuffle($permitted_chars), 0, 16);
						$encrypt = $username.$password.$key;
						$encrypted = password_hash($encrypt, PASSWORD_DEFAULT);
						$create_at = new DateTime();
						$create_at = $create_at->format('Y-m-d H:i:s');
						$cek_user = $this->M_Api->cek_user($username)->row_array();
						$cek_email = $this->M_Api->cek_email($email)->row_array();

						if($cek_user['count'] == '0') {
							if($cek_email['count'] == '0') {
								$data = [
									'username'  => $username,
									'email'		=> $email,
									'password'  => $encrypted,
									'name'      => $name,
									'key'       => $key,
									'created_at' => $create_at
								];

								$this->M_Api->insert_data(Auth::$table, $data);

								$respone['value'] = "1";
								$respone['message'] = "Sukses";
								$respone['username'] = $username;
								$respone['email'] = $email;
								$respone['name'] = $name;
								
								
							} else {
								$respone['value'] = "0";
								$respone['message'] = "Email telah di gunakan";
							}

						} else {
							$respone['value'] = "0";
							$respone['message'] = "Username telah di gunakan";
						}
					}
				}
			}
			
		} else {
            $respone['value'] = "0";
            $respone['message'] = "invalid request method";
        }

        $this->json_respone($respone);
    } 
	
	public function login () {

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
			
			$post = (array)json_decode(file_get_contents('php://input'));
			if(empty($post)) {
				$respone['value'] = "0";
				$respone['message'] = "Form harus di isi lengkap";
			} else {
				$username = trim($post['username']);
				$password = trim($post['password']);
				
				if(empty($username)) {
					$respone['value'] = "0";
					$respone['message'] = "Username harus di isi";
				} elseif (empty($password)) {
					$respone['value'] = "0";
					$respone['message'] = "Password harus di isi";
				} else {

					if(strlen($username) >= 0 && strlen($username) < 6) {
						$respone['value'] = "0";
						$respone['message'] = "Username terlalu pendek";

					} elseif (strlen($username) >= 0 && strlen($username) > 20) {
						$respone['value'] = "0";
						$respone['message'] = "Username terlalu panjang";

					} elseif (strlen($password) >= 0 && strlen($password) < 6) {
						$respone['value'] = "0";
						$respone['message'] = "Password terlalu pendek";

					} elseif (strlen($password) >= 0 && strlen($password) > 20) {
						$respone['value'] = "0";
						$respone['message'] = "Password terlalu panjang";

					} else {

						$cek = $this->M_Api->cek_user($username)->row_array();

						if($cek['count'] != '0') {
							
							$user = $this->M_Api->get_data_byid(Auth::$table, $username, 'username')->row_array();
							$key = $user['key'];
							$pass = $user['password'];
							$encrypt = $username.$password.$key;						
						
							if(password_verify($encrypt, $pass)){

								$name = $user['name'];
								$role = $user['roles'];
								$email = $user['email'];
								$username = $user['username'];
								
								/* $data = [
									'name'      => $name,
									'email'		=> $email,
									'username'  => $username,
									'role'      => $role
									
								]; */

								//$access_token = JWT::encode($data, $secret_key, 'HS256');
								//$data['access_token'] = $access_token;
								
								$respone['value'] = "1";
								$respone['message'] = "Sukses";
								$respone['name'] = $name;
								$respone['email'] = $email;
								$respone['username'] = $username;
								$respone['role'] = $role;
				
							} else {
								$respone['value'] = "0";
								$respone['message'] = "Password salah";
							};                

						} else {
							$respone['value'] = "0";
							$respone['message'] = "User tidak ditemukan";        
						};
					}
				}
			}
        } else {
            $respone['value'] = "0";
            $respone['message'] = "invalid request method";
        }
		
        $this->json_respone($respone);

    }
}